---------------------------------------------------------------------------------------------------
Version: 1.3.15
Date: 3.25.2021
  Features:
    - Preformance optimizations
---------------------------------------------------------------------------------------------------
Version: 1.3.12
Date: 10.10.2020
  Features:
    - Added compatiblity with Picker Dollies
    - Memory elements and memory communicators will now appear in game even if you do not have the main mod installed
    - Moved all items into a seperate subgroup
---------------------------------------------------------------------------------------------------
Version: 1.3.11
Date: 10.9.2020
  Features:
    - Fixed a bug that was causing power usage to be lower than intended
---------------------------------------------------------------------------------------------------
Version: 1.3.10
Date: 10.8.2020
  Features:
    - Added a low-res variant of the fluid flow animation
    - Added a library module. This will make it easier for me to apply changes to this and the main mod
    - Removed vanilla fluid flow animation. Will improve FPS
---------------------------------------------------------------------------------------------------
Version: 1.3.9
Date: 10.7.2020
  Features:
    - Fixed a bug that caused units to waste FPS when they were empty
---------------------------------------------------------------------------------------------------
Version: 1.3.8
Date: 10.4.2020
  Features:
    - Space exploration compaiblity
---------------------------------------------------------------------------------------------------
Version: 1.3.7
Date: 9.26.2020
  Features:
    - Fluid units will now work if they are above 90% power, instead of having to be 100%
    - Fixed empty memory element cycling
    - Fixed a crash when you removed a mod that added a fluid that was stored in a unit
---------------------------------------------------------------------------------------------------
Version: 1.3.6
Date: 9.18.2020
  Features:
    - Fixed units getting stuck sometimes
---------------------------------------------------------------------------------------------------
Version: 1.3.5
Date: 9.17.2020
  Features:
    - Decreased internal storage tank size from 1M to 120k. This will make it more responsive to running out of power
---------------------------------------------------------------------------------------------------
Version: 1.3.4
Date: 9.17.2020
  Features:
    - New mod description
    - Tempature is now calcuated in the background. The text display was getting to long.
    - Fixed a tempature dupe involving empty memory elements
---------------------------------------------------------------------------------------------------
Version: 1.3.3
Date: 9.17.2020
  Features:
    - You can now blueprint fluid units
---------------------------------------------------------------------------------------------------
Version: 1.3.2
Date: 9.16.2020
  Features:
    - Improved error handling when a unit is corrupted
---------------------------------------------------------------------------------------------------
Version: 1.3.1
Date: 9.15.2020
  Features:
    - Increased update rate from 4 sec to 1 sec
---------------------------------------------------------------------------------------------------
Version: 1.3.0
Date: 9.12.2020
  Features:
    - The indicator text will now fit better on the entity
    - FPS improvements
    - Added map icons
    - Added empty memory elements
    - Added an icon when the unit is on low power
    - Improve color for fluid animation
    - Fixed a crash with mini-tutorials
---------------------------------------------------------------------------------------------------
Version: 1.2.3
Date: 9.11.2020
  Features:
    - Changed thumbnail for real
---------------------------------------------------------------------------------------------------
Version: 1.2.2
Date: 9.11.2020
  Features:
    - Added support for cloning the unit. A stepping stone to full space exploration compatiblity
    - Changed thumbnail
---------------------------------------------------------------------------------------------------
Version: 1.2.1
Date: 9.11.2020
  Features:
    - Fixed a crash
---------------------------------------------------------------------------------------------------
Version: 1.2.0
Date: 9.10.2020
  Features:
    - Fixed that units would sometimes go inactive
    - Changed some descriptions
    - Tempature is now stored
    - You can no longer fill the units with inserters or loaders
    - Increased power usage
    - Space ex compaiblity
    - Fluid texture is now animated!
---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 9.9.2020
  Features:
    - Removed dependancy on main mod
    - Fixed a crash
    - Fixed an issue with the fluid display
    - You can now use memory elements with fluid units
    - Adjusted pressure
    - Reduced power usage
    - Added a setting to adjust power usage
    - Fixed some issues with shadows
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 9.2.2020
  Features:
    - Initial release
